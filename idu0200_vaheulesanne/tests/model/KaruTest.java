package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class KaruTest extends Karu {

	@Test
	public void testGetTyyp() {
		setTyyp("Loll");
		assertEquals("Loll", getTyyp());
	}

	@Test
	public void testGetVanus() {
		setVanus(123);
		assertEquals((Integer) 123, getVanus());
	}

	@Test
	public void testGetKirjeldus() {
		setKirjeldus("See karu on loll");
		assertEquals("See karu on loll", getKirjeldus());
	}

}
