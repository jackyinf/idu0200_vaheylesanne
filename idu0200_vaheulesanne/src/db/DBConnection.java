package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class DBConnection {

	/**
	 * Connects to database using properties defined in
	 * WebContent/WEB-INF/classes/DBConnection.properties
	 * 
	 * @return Connection
	 */
	public static Connection getConnection() {
		Connection db_connection = null;
		String pwd = "";
		String usr = "";
		String url = "";
		String prefix = "local";

		try {
			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection");
			Class.forName(bundle.getString("Driver"));
			url = bundle.getString(prefix + "url");
			usr = bundle.getString(prefix + "usr");
			pwd = bundle.getString(prefix + "pwd");
			db_connection = DriverManager.getConnection(url, usr, pwd);

		} catch (Exception ex) {
			System.out.println("DBConnection.getConnection(): " + ex.getMessage());
		}
		return db_connection;
	}

	/**
	 * Closes the connection
	 * 
	 * @param connection
	 */
	public static void close(final Connection connection) {

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException ex) {
				System.out.println("DBConnection.close(): " + ex.getMessage());
			}
		}

	}

	/**
	 * 
	 * @param statement
	 */
	public static void closeStatement(final Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException ex) {
				System.out.println("DBConnection.closeStatement()"
						+ ex.getMessage());
			}
		}
	}

	/**
	 * 
	 * @param resultSet
	 */
	public static void closeResultSet(final ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException ex) {
				System.out.println("DBConnection.closeResult()"
						+ ex.getMessage());
			}
		}
	}

}
