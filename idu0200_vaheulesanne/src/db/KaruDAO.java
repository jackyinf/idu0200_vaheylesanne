package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Karu;

public class KaruDAO {

	/*
	 * DECLARATIONS
	 */

	private Connection connection;
	private String sql;
	private ResultSet resultSet;
	private PreparedStatement preparedStatement;

	/**
	 * 
	 * @return K�ikide karude nimekiri
	 */
	public ArrayList<Karu> getAll() {
		ArrayList<Karu> karud = new ArrayList<Karu>();
		
		try {
			connection = DBConnection.getConnection();
			sql = "SELECT * FROM karu";
			preparedStatement = connection.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
				//TODO: get karud
			}
			
		} catch (Exception ex) {
			System.out
			.println("DocumentModel.addDocument(document, category, documentType): "
					+ ex.getMessage());
		} finally {
			DBConnection.closeStatement(preparedStatement);
			DBConnection.closeResultSet(resultSet);
			DBConnection.close(connection);
		}

		return karud;
	}
	
}
