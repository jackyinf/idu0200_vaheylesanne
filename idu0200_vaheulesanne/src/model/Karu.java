package model;

public class Karu {

	private String tyyp;
	private Integer vanus;
	private String kirjeldus;
	
	/*
	 * GETTERS
	 */
	
	public String getTyyp() {
		return tyyp;
	}
	
	public Integer getVanus() {
		return vanus;
	}
	
	public String getKirjeldus() {
		return kirjeldus;
	}
	
	/*
	 * SETTERS
	 */

	public void setTyyp(String tyyp) {
		this.tyyp = tyyp;
	}
	
	public void setVanus(Integer vanus) {
		this.vanus = vanus;
	}
	
	public void setKirjeldus(String kirjeldus) {
		this.kirjeldus = kirjeldus;
	}
	
}
